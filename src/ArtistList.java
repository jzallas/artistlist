import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Jonathan Zallas
 * @version 1.0.1
 * @since 3/5/2014
 */

public class ArtistList {

	final static int MIN_NUMBER_OF_OCCURRENCES = 50;
	final static String IN_FILE_NAME = "Artist_lists_small.txt";
	final static String OUT_FILE_NAME = "output.txt";
	final static Charset ENCODING = StandardCharsets.UTF_8;
	/**
	 * A HashMap where the artist's name is the <em>key</em> and the
	 * <em>value</em> is an ArrayList of Integers. The ArrayList holds a list of
	 * line numbers that the artist was found on. Coincidentally, each line
	 * corresponds to a user. Therefore, it is a list of users that the artist
	 * was found on.
	 */
	HashMap<String, ArrayList<Integer>> artists = new HashMap<String, ArrayList<Integer>>();

	public static void main(String[] args) {
		ArtistList test = new ArtistList();
		long start = System.nanoTime();

		try {
			test.generate(IN_FILE_NAME, OUT_FILE_NAME,
					MIN_NUMBER_OF_OCCURRENCES);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Completed in "
				+ ((double) (System.nanoTime() - start) / 1000000000.0)
				+ " seconds.");
	}

	/**
	 * Using an input file <em>inFileName</em>, this will produce an output file
	 * <em>outFileName</em> containing a list of pairs of artists which appear
	 * TOGETHER in at least <em>minNumberOfPairOccurrences</em> different lists
	 * 
	 * @param inFileName
	 *            Input file name as a String
	 * @param outFileName
	 *            Output file name as a String
	 * @param minNumberOfPairOccurrences
	 *            The minimum number of lists in which artists appear TOGETHER
	 *            in
	 * @throws IOException
	 *             if an I/O error occurs opening source
	 */
	public void generate(String inFileName, String outFileName,
			int minNumberOfPairOccurrences) throws IOException {
		readTextFile(IN_FILE_NAME);
		writeTextFile(OUT_FILE_NAME, getPairs(minNumberOfPairOccurrences));

	}

	/**
	 * Will first filter and then extract all the pairs from the Hashmap of
	 * artists and their corresponding user ArrayLists.
	 * 
	 * @param minNumberOfPairOccurrences
	 *            The minimum number of lists in which artists appear TOGETHER
	 *            in
	 * @return ArrayList of Strings, each string contains a pair like
	 *         "artist1, artist2"
	 * @see filter( )
	 */
	private ArrayList<String> getPairs(int minNumberOfPairOccurrences) {
		filter(minNumberOfPairOccurrences);
		ArrayList<String> allPairs = new ArrayList<>();
		for (Iterator<Map.Entry<String, ArrayList<Integer>>> it = artists
				.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, ArrayList<Integer>> entry = it.next();
			String artistName = entry.getKey();
			ArrayList<Integer> usersList = entry.getValue();
			it.remove();
			for (String key : artists.keySet()) {
				int numberOfPairs = intersectionOf(usersList, artists.get(key));
				if (numberOfPairs >= minNumberOfPairOccurrences)
					allPairs.add(artistName + ", " + key);
			}
		}

		return allPairs;
	}

	/**
	 * @param usersListFromFirstArtist
	 *            An ArrayList of Integers containing all the users which have
	 *            this artist on their list.
	 * @param usersListFromSecondArtist
	 *            An ArrayList of Integers containing all the users which have
	 *            this artist on their list.
	 * @return The number of users that have both the first and second artist
	 *         TOGETHER on their lists.
	 */
	private int intersectionOf(ArrayList<Integer> usersListFromFirstArtist,
			ArrayList<Integer> usersListFromSecondArtist) {
		if (usersListFromFirstArtist.isEmpty()
				&& usersListFromSecondArtist.isEmpty())
			return 0;
		ArrayList<Integer> intersection = new ArrayList<Integer>(
				usersListFromFirstArtist);
		intersection.retainAll(usersListFromSecondArtist);
		return intersection.size();
	}

	/**
	 * If the artist does not appear at least the minimum number of times then
	 * the artist is ineligible to appear in the minimum number of pairs.
	 * Therefore, this removes those artists with less than the minimum so as
	 * not to waste time counting ineligible artists
	 * 
	 * @param minNumberOfPairOccurences
	 *            The minimum number of lists in which artists appear TOGETHER
	 *            in
	 */
	private void filter(int minNumberOfPairOccurences) {
		for (Iterator<Map.Entry<String, ArrayList<Integer>>> it = artists
				.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, ArrayList<Integer>> entry = it.next();
			if (entry.getValue().size() < minNumberOfPairOccurences)
				it.remove();
		}
	}

	/**
	 * Reads the specified file and loads everything into a HashMap for later
	 * parsing.
	 * 
	 * @param inFileName
	 *            Input file name as a String
	 * @throws IOException
	 *             if an I/O error occurs opening source
	 */
	private void readTextFile(String inFileName) throws IOException {
		Path path = Paths.get(inFileName);
		int lineNumber = 0;
		try (Scanner scanner = new Scanner(path, ENCODING.name())) {
			while (scanner.hasNextLine()) {
				lineNumber++;
				// process each line in some way
				String[] line = String.valueOf(scanner.nextLine()).split(",");
				for (String artistName : line) {
					if (!artists.containsKey(artistName.trim())) {
						ArrayList<Integer> user = new ArrayList<>();
						user.add(lineNumber);
						artists.put(artistName.trim(), user);
					} else {
						artists.get(artistName.trim()).add(lineNumber);
					}
				}
			}
		}
	}

	/**
	 * Writes the file to the disk and outputs a System Message stating the
	 * directory in which it was written to.
	 * 
	 * @param outFileName
	 *            Output file name as a String
	 * @param pairs
	 *            An ArrayList of the pairs that were generated.
	 * @throws IOException
	 */
	private void writeTextFile(String outFileName, ArrayList<String> pairs)
			throws IOException {
		Path path = Paths.get(outFileName);
		System.out.println("Output file written to "
				+ path.toAbsolutePath().toString());
		try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
			for (String pair : pairs) {
				writer.write(pair);
				writer.newLine();
			}
		}
	}
}
